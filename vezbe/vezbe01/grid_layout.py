from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QLabel
from PyQt5.QtGui import QIcon, QPixmap
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Grid layout")
        self.setWindowIcon(QIcon("smiley.png"))

        label1 = QLabel(self)
        label2 = QLabel(self)
        label3 = QLabel(self)
        label4 = QLabel(self)

        label1.setPixmap(QPixmap("Smiley_green_alien.svg.png"))
        label2.setPixmap(QPixmap("Smiley_green_alien_wow.svg.png"))
        label3.setPixmap(QPixmap("Smiley_green_alien_black_ninja.svg.png"))
        label4.setPixmap(QPixmap("Smiley_green_alien_big_eyes.svg.png"))

        grid = QGridLayout()
        grid.addWidget(label1, 0, 0)
        grid.addWidget(label2, 1, 0)
        grid.addWidget(label3, 0, 1)
        grid.addWidget(label4, 1, 1)

        # grid.addWidget(label1, 0, 0)
        # grid.addWidget(label2, 0, 1)
        # grid.addWidget(label3, 0, 2)
        # grid.addWidget(label4, 0, 3)

        # grid.addWidget(label1, 0, 0)
        # grid.addWidget(label2, 1, 0)
        # grid.addWidget(label3, 2, 0)
        # grid.addWidget(label4, 3, 0)
        self.setLayout(grid)

        self.resize(300, 254)
        self.show()

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())