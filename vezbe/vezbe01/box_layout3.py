from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QPushButton
from PyQt5.QtGui import QIcon
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Box layout3")
        self.setWindowIcon(QIcon("smiley.png"))

        btn1 = QPushButton("Button 1", self)
        btn2 = QPushButton("Button 2", self)
        btn3 = QPushButton("Button 3", self)
        btn4 = QPushButton("Button 4", self)

        vbox = QVBoxLayout()
        vbox.addWidget(btn1)
        vbox.addStretch()
        vbox.addWidget(btn2)
        vbox.addStretch()

        hbox = QHBoxLayout()
        hbox.addWidget(btn3)
        hbox.addStretch()
        hbox.addWidget(btn4)

        vbox.addLayout(hbox)
        self.setLayout(vbox)

        self.resize(300, 254)
        self.show()

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())