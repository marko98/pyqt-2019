from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QPushButton
from PyQt5.QtGui import QIcon
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Box layout2")
        self.setWindowIcon(QIcon("smiley.png"))

        btn1 = QPushButton("Button 1", self)
        btn2 = QPushButton("Button 2", self)

        hbox = QHBoxLayout()
        hbox.addWidget(btn1)
        hbox.addStretch()
        hbox.addWidget(btn2)
        self.setLayout(hbox)

        self.resize(300, 254)
        self.show()

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())