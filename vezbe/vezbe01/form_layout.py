from PyQt5.QtWidgets import QApplication, QWidget, QFormLayout, QSpinBox, QLineEdit
from PyQt5.QtGui import QIcon, QPixmap
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Form layout")
        self.setWindowIcon(QIcon("smiley.png"))

        nameLineEdit = QLineEdit()
        emailLineEdit = QLineEdit()
        ageSpinBox = QSpinBox()

        form = QFormLayout()
        form.addRow("Name:", nameLineEdit)
        form.addRow("Email:", emailLineEdit)
        form.addRow("Age:", ageSpinBox)
        self.setLayout(form)

        self.resize(300, 254)
        self.show()

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())