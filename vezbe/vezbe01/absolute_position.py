from PyQt5.QtWidgets import QApplication, QWidget, QLabel 
from PyQt5.QtGui import QIcon
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Absolute position")
        self.setWindowIcon(QIcon("smiley.png"))

        label1 = QLabel(parent = self)
        label1.setText("Don\'t")
        label1.move(80, 50)

        label2 = QLabel(self)
        label2.setText("use")
        label2.move(90, 80)

        label3 = QLabel("this positioning technique in real life!", self)
        label3.move(100, 110)        

        self.resize(300, 254)
        self.show()

if __name__ == '__main__':    
    q_app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(q_app.exec_())