from PyQt5.QtWidgets import QApplication, QWidget
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Primer 00")
        self.resize(230, 254)
        self.show()


if __name__ == '__main__':    
    q_app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(q_app.exec_())
