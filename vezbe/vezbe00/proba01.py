from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtGui import QIcon
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Primer 00")

        self.setWindowIcon(QIcon("smiley.png"))

        push_button = QPushButton(parent = self)
        push_button.setText("Quit")
        push_button.move(110, 50)
        push_button.clicked.connect(QApplication.instance().quit)

        self.resize(300, 254)
        self.show()


if __name__ == '__main__':    
    q_app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(q_app.exec_())
