from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton, QMessageBox, QLineEdit, QFormLayout
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import pyqtSlot
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Signals example02")
        self.setWindowIcon(QIcon("smiley.png"))

        self.name_line_edit = QLineEdit()
        self.submit_button = QPushButton("&Submit")

        form_layout = QFormLayout()
        form_layout.addRow("Name:", self.name_line_edit)

        v_box = QVBoxLayout()
        v_box.addLayout(form_layout)
        v_box.addWidget(self.submit_button)

        self.submit_button.clicked.connect(self.submit_contact)

        self.setLayout(v_box)
        self.resize(300, 254)
        self.show()

    def submit_contact(self):
        name = self.name_line_edit.text()

        if name == "":
            QMessageBox.critical(self, "Empty Field", "Please enter a name.")
        else:
            QMessageBox.information(self, "Success!", "Hello %s!" % name)

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())