from PyQt5.QtWidgets import QApplication, QWidget, QFormLayout, QSpinBox, QLineEdit
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import pyqtSlot
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Signals example00")
        self.setWindowIcon(QIcon("smiley.png"))

        name_line_edit = QLineEdit()
        email_line_edit = QLineEdit()
        age_spin_box = QSpinBox()

        form_layout = QFormLayout()
        form_layout.addRow("Name:", name_line_edit)
        form_layout.addRow("Email:", email_line_edit)
        form_layout.addRow("Age:", age_spin_box)
        
        name_line_edit.editingFinished.connect(self.name_finished)
        name_line_edit.textChanged[str].connect(self.show_name)
        age_spin_box.valueChanged[int].connect(self.headsUp)

        self.setLayout(form_layout)
        self.resize(300, 254)
        self.show()

    @pyqtSlot(str)
    def show_name(self, name):
        print("Name is:", name)
    
    def name_finished(self):
        print("Name is written.")

    @pyqtSlot(int)
    def headsUp(self, arg1):
        print("New value is:", arg1)

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())