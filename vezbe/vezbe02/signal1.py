from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLCDNumber, QDial
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import pyqtSlot
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle("Signals example01")
        self.setWindowIcon(QIcon("smiley.png"))

        lcd = QLCDNumber(self)
        dial = QDial(self)
        dial.setMinimum(50)

        v_box = QVBoxLayout()
        v_box.addWidget(lcd)
        # v_box.addStretch()
        v_box.addWidget(dial)

        dial.valueChanged.connect(lcd.display)

        self.setLayout(v_box)
        self.resize(300, 254)
        self.show()

if __name__ == "__main__":
    application = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(application.exec_())