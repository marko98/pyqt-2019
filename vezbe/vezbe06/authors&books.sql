-- MySQL Script generated by MySQL Workbench
-- Sun Feb 10 10:45:44 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema authors&books
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema authors&books
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `authors&books` DEFAULT CHARACTER SET utf8 ;
USE `authors&books` ;

-- -----------------------------------------------------
-- Table `authors&books`.`Authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authors&books`.`Authors` (
  `autor_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`autor_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `authors&books`.`Books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authors&books`.`Books` (
  `book_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `author` INT NOT NULL,
  PRIMARY KEY (`book_id`),
  INDEX `fk_books_author_idx` (`author` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  CONSTRAINT `fk_books_author`
    FOREIGN KEY (`author`)
    REFERENCES `authors&books`.`Authors` (`autor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `authors&books`.`Authors`
-- -----------------------------------------------------
START TRANSACTION;
USE `authors&books`;
INSERT INTO `authors&books`.`Authors` (`autor_id`, `first_name`, `last_name`) VALUES (1, 'Rik', 'Riordan');
INSERT INTO `authors&books`.`Authors` (`autor_id`, `first_name`, `last_name`) VALUES (2, 'Kon', 'Igulden');
INSERT INTO `authors&books`.`Authors` (`autor_id`, `first_name`, `last_name`) VALUES (3, 'Tom', 'Klensi');

COMMIT;


-- -----------------------------------------------------
-- Data for table `authors&books`.`Books`
-- -----------------------------------------------------
START TRANSACTION;
USE `authors&books`;
INSERT INTO `authors&books`.`Books` (`book_id`, `name`, `author`) VALUES (1, 'Titanova kletva', 1);
INSERT INTO `authors&books`.`Books` (`book_id`, `name`, `author`) VALUES (2, 'Bitka za lavirint', 1);
INSERT INTO `authors&books`.`Books` (`book_id`, `name`, `author`) VALUES (3, 'Imperator', 2);
INSERT INTO `authors&books`.`Books` (`book_id`, `name`, `author`) VALUES (4, 'Zavrsni udarac', 3);

COMMIT;

