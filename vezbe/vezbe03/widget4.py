from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QProgressBar
from PyQt5.QtCore import pyqtSlot, QTimeLine
import sys

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):        
        self.setWindowTitle('QProgressBar demo')      

        self.lambda_button = QPushButton("Primer lambde(anonimne f-je)", self)
        self.lambda_button.clicked.connect(self.primer_lambde)
       
        self.timerButton = QPushButton("Start", self)
        self.timerButton.clicked.connect(self.timerStart)

        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(10, 20, 290, 25)                

        self.timerObject = QTimeLine(2000, self) # 2s traje ceo proces
        self.timerObject.setFrameRange(0, 100) # za 2s mora da dodje od frejma 0 do frejma 100
        # Every 2000/100 milliseconds, it will move to the next frame of the animation.

        # Lambdas are one line functions. They are also known as anonymous functions in some languages.
        # You use lambdas when you don’t want to call a function twice in a program. They are just like normal functions and even behave like them.
        # detaljnije -> Chang, Edward. Newbie PyQt5: A Beginner’s guide to PyQt5 (Page 34).  . Kindle Edition. 
        self.timerObject.finished.connect(lambda: self.timerButton.setText("Finished"))

        self.timerObject.frameChanged[int].connect(self.frame)
        self.timerObject.frameChanged[int].connect(lambda: self.jej("vuhu radi!!!"))
        self.timerObject.frameChanged.connect(self.progressBar.setValue)

        self.timerButton.move(110,150)      
        self.lambda_button.move(70,200)   
        self.progressBar.move(10,100)

        self.resize(300, 300)
        self.show()

    def jej(self, text):
        print(text)

    def primer_lambde(self):
        add = lambda x, y : x + y
        print(add(1,9))

    @pyqtSlot(int)
    def frame(self, arg1):
        print("Frejm:", arg1)
        print("Stanje objekta timerObject:", self.timerObject.state())
        print("Stanje objekta timerObject:", self.timerObject.state())
        

    @pyqtSlot()
    def timerStart(self):
        if (self.timerObject.state() == QTimeLine.Running): # proverava se da li je stanje objekta timerObject koji pripada klasi QTimeLine odgovara stanju running(2) iz te klase
            self.timerObject.stop()
            self.timerButton.setText("Resume")
        else:            
            self.timerButton.setText("Pause")
            self.timerObject.resume()
        print("Stanje objekta timerObject:", self.timerObject.state())

if __name__ == '__main__':    
    qApp = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(qApp.exec_())
