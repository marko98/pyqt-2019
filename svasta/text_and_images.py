# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Win10\Desktop\PyQt5 - proba\text_and_images.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(435, 424)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButtonSelectImage = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonSelectImage.setGeometry(QtCore.QRect(20, 70, 75, 23))
        font = QtGui.QFont()
        font.setFamily("Segoe Print")
        self.pushButtonSelectImage.setFont(font)
        self.pushButtonSelectImage.setObjectName("pushButtonSelectImage")
        self.labelImage = QtWidgets.QLabel(self.centralwidget)
        self.labelImage.setGeometry(QtCore.QRect(106, 10, 311, 161))
        self.labelImage.setFrameShape(QtWidgets.QFrame.Box)
        self.labelImage.setText("")
        self.labelImage.setObjectName("labelImage")
        self.lineEditText = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditText.setGeometry(QtCore.QRect(20, 220, 131, 20))
        self.lineEditText.setObjectName("lineEditText")
        self.pushButtonAddText = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonAddText.setGeometry(QtCore.QRect(20, 240, 131, 23))
        font = QtGui.QFont()
        font.setFamily("Rockwell Extra Bold")
        font.setBold(True)
        font.setWeight(75)
        self.pushButtonAddText.setFont(font)
        self.pushButtonAddText.setObjectName("pushButtonAddText")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(160, 190, 256, 192))
        self.listWidget.setObjectName("listWidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 435, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButtonSelectImage.clicked.connect(self.select_image)
        self.pushButtonAddText.clicked.connect(self.add_text)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButtonSelectImage.setText(_translate("MainWindow", "Select Image"))
        self.pushButtonAddText.setText(_translate("MainWindow", "Add"))

    def select_image(self):
        #  _ je promenjiva
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(None, "select Image", "", "Image Files (*.png *.jpg *jpeg *.bpm)")
        if file_name:
            pixmap = QtGui.QPixmap(file_name)
            pixmap = pixmap.scaled(self.labelImage.width(), self.labelImage.height(), QtCore.Qt.KeepAspectRatio)
            self.labelImage.setPixmap(pixmap)
            self.labelImage.setAlignment(QtCore.Qt.AlignCenter)

    def add_text(self):
        value = self.lineEditText.text()
        self.lineEditText.clear()
        self.listWidget.addItem(value)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

