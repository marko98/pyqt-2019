import sys
from PyQt5 import QtWidgets, QtGui, QtCore, Qt

# ------------------------------------------------
from create_window import Window
# ------------------------------------------------

# --------------- pocetak aplikacije -------------
app = QtWidgets.QApplication(sys.argv)

main_window = Window("The Library", "background-color:#cceeff")
main_window.set_coordinates()

# bibliotekar_window = Window("Bibliotekar", "background-color:#cceeff")
# kupac_window = Window("Kupac", "background-color:#cceeff")

# --------------- kraj aplikacije ----------------
sys.exit(app.exec_())