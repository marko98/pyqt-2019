import sys
from PyQt5 import QtWidgets, QtGui, QtCore, Qt

class Window:
    def __init__(self, name, background_color = "background-color:white", x = 500, y = 200, width = 300, height = 600):
        self.window = QtWidgets.QMainWindow()

        self.window.setWindowTitle(name)
        self.window.setGeometry(x, y, width, height)
        self.window.setStyleSheet(background_color)

        self.window.show()

    def hide(self):
        self.window.hide()

    def show(self):
        self.window.show()
    
    def set_coordinates(self):
        print(self.window.geometry())

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    # let's instantiate an object to the class mainWindow
    window = Window("Window")
    sys.exit(app.exec_())