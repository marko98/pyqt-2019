import sys
from PyQt5 import QtWidgets, QtGui, QtCore, Qt

class MainWindow:
    def __init__(self):
        self.app = QtWidgets.QApplication(sys.argv)
        self.window = QtWidgets.QMainWindow()

        # the path of the image
        self.image_path = "images/user.png"

        self.stylesheet = """
        QPushButton{
            background-color:#80bfff; 
            color: #fafafa; 
            font-size: 15px; 
            border: 1px dotted #0059b3
        }
        QTextEdit#username{
            background-color:#b3d9ff; 
            color: #fafafa;
            padding-top: 7px; 
            padding-left: 10px
        }
        QTextEdit{
            background-color:#e6f7ff; 
            color: #8e8e8e; 
            padding-top: 7px; 
            padding-left: 10px
        }
        QLabel{
            color: #8e8e8e;
        }
        QLabel#labelImage{
            background-color:#b3d9ff;
            border-radius: 50px 20px
        }
        QMainWindow{
            background-color:#cceeff
        }
        """

        self.init_gui()

        self.window.setWindowTitle("Create Account")
        self.window.setGeometry(500, 200, 300, 600)
        # self.window.setStyleSheet("background-color:#cceeff")

        self.window.show()
        self.app.setStyleSheet(self.stylesheet)
        sys.exit(self.app.exec_())

    # create a function to initialize the Gui
    def init_gui(self):
        # create a label that contains the image
        self.image = QtGui.QImage(self.image_path)

        # put label on window
        self.label = QtWidgets.QLabel(self.window)
        self.label.setGeometry(50, 20, 200, 200)
        # self.label.setStyleSheet("background-color:#cceeff")
        self.label.setPixmap(QtGui.QPixmap.fromImage(self.image))
        # scale the content
        self.label.setScaledContents(True)
        self.label.setObjectName("labelImage")

        # # create a pseudo field
        # self.pseudo = QtWidgets.QTextEdit(self.window)
        # self.pseudo.setGeometry(25, 278, 250, 40)
        # # self.pseudo.setStyleSheet("background-color:#e6f7ff; color: #8e8e8e; padding-top: 7px; padding-left: 10px")
        # self.pseudo.setObjectName("pseudo")
        # self.pseudo.setText("pseudo")

        # create a username field
        self.username = QtWidgets.QTextEdit(self.window)
        self.username.setGeometry(25, 278, 250, 40)
        # self.username.setStyleSheet("background-color:#e6f7ff; color: #8e8e8e; padding-top: 7px; padding-left: 10px")
        self.username.setObjectName("username")
        self.username.setText("username")

        # create the email field
        self.email = QtWidgets.QTextEdit(self.window)
        self.email.setGeometry(25, 330, 250, 40)
        # self.email.setStyleSheet("background-color:#e6f7ff; color: #8e8e8e; padding-top: 7px; padding-left: 10px")
        self.email.setObjectName("email")
        self.email.setText("Email")

        # create the password field
        self.password = QtWidgets.QTextEdit(self.window)
        self.password.setGeometry(25, 390, 250, 40)
        # self.password.setStyleSheet("background-color:#e6f7ff; color: #8e8e8e; padding-top: 7px; padding-left: 10px")
        self.password.setObjectName("password")
        self.password.setText("Password")

        # create the confirm password field
        self.confirm_password = QtWidgets.QTextEdit(self.window)
        self.confirm_password.setGeometry(25, 450, 250, 40)
        # self.confirm_password.setStyleSheet("background-color:#e6f7ff; color: #8e8e8e; padding-top: 7px; padding-left: 10px")
        self.confirm_password.setObjectName("confirm_password")
        self.confirm_password.setText("Confirm password")

        # create the register button
        self.register_btn = QtWidgets.QPushButton(self.window)
        self.register_btn.setText("Create an Account")
        self.register_btn.setGeometry(25, 510, 250, 40)
        # self.register_btn.setStyleSheet("background-color:#80bfff; color: #fafafa; font-size: 15px; border: 1px dotted #0059b3")
        self.register_btn.setObjectName("register_btn")

        self.message = QtWidgets.QLabel(self.window)
        self.message.setGeometry(0, 550, 250, 40)
        self.message.setObjectName("message")
        self.message.setAlignment(QtCore.Qt.AlignCenter)

        self.register_btn.clicked.connect(self.register)

    def register(self):
        username = self.username.toPlainText()
        # self.username.clear()
        email = self.email.toPlainText()
        # self.email.clear()
        password = self.password.toPlainText()
        # self.password.clear()
        confirm_password = self.confirm_password.toPlainText()
        # self.confirm_password.clear()
        # print(username, email, password, confirm_password)

        with open("users.txt", "r", encoding="utf-8") as r:
            lines = r.readlines()
            for data in lines:
                user_data = data.split(",")
                user_username = user_data[0]
                if username == user_username:
                    self.message.setText("username is taken")
                    return
        
        if not confirm_password == password:
            self.message.setText("password error")
            return

        self.message.clear()
        with open("users.txt", "a", encoding="utf-8") as w:
            w.write(username + "," + password + "," + email + "\n")
    
    def hide(self):
        self.window.hide()
    
    def show(self):
        self.window.show()

    def set_coordinates(self, x, y):    
        # self.window.setGeometry(x, y, 600, 600)
        print("geometry:", self.window.geometry())

if __name__ == '__main__':
    # let's instantiate an object to the class mainWindow
    window = MainWindow()