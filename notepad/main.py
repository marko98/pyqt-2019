import sys
import os
from PyQt5 import QtWidgets, QtGui

class Notepad(QtWidgets.QMainWindow):
    def __init__(self):
        super(Notepad, self).__init__()
        self.init_UI()

        # ovde je self = QtWidgets.QMainWindow()

        self.setCentralWidget(self.text)        
        self.setGeometry(300,300,450,450)
        self.setWindowTitle('Notepad')
        self.show()

    def init_UI(self):
        # QAction? It is a widget for Qt that acts as an option in a menu
        # his parent is self(window)
        close_notepad_action = QtWidgets.QAction("Close", self)
        close_notepad_action.setShortcut("Ctrl+Q")
        close_notepad_action.setStatusTip("Close Notepad")
        close_notepad_action.triggered.connect(self.close)

        # his parent is self(window)
        self.text = QtWidgets.QTextEdit(self)
        self.setCentralWidget(self.text)

        new_file_action = QtWidgets.QAction("New", self)
        new_file_action.setShortcut("Ctrl+N")
        new_file_action.setStatusTip("Create new file")
        new_file_action.triggered.connect(self.new_file)

        save_file_action = QtWidgets.QAction("Save", self)
        save_file_action.setShortcut("Ctrl+S")
        save_file_action.setStatusTip("Save current file")
        save_file_action.triggered.connect(self.save_file)

        open_file_action = QtWidgets.QAction("Open", self)
        open_file_action.setShortcut("Ctrl+O")
        open_file_action.setStatusTip("Open a file")
        open_file_action.triggered.connect(self.open_file)

        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu("&File")
        file_menu.addAction(close_notepad_action)

        file_menu.addAction(new_file_action)
        file_menu.addAction(save_file_action)
        file_menu.addAction(open_file_action)

    def new_file(self):
        self.text.clear()

    def save_file(self):
        file_name, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save File", os.getenv("HOME"))
        # print("->"+file_name+"<-")
        if file_name != "":
            with open(file_name+".txt", "w") as w:
                w.write(self.text.toPlainText())

    def open_file(self):
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open File", os.getenv("HOME"))
        # print(file_name)
        with open(file_name, "r") as r:
            file_data = r.read()
            self.text.setText(file_data)

def main():
    # sets up our application to run and grabs any command line arguments to use with the application
    app = QtWidgets.QApplication(sys.argv)
    notepad = Notepad()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()