import sys
import os
from PyQt5 import QtWidgets

class Notepad(QtWidgets.QMainWindow):
    def __init__(self):
        super(Notepad, self).__init__()        
        self.initUI()
         
    def initUI(self):
        newAction = QtWidgets.QAction('New', self)
        newAction.setShortcut('Ctrl+N')
        newAction.setStatusTip('Create new file')
        newAction.triggered.connect(self.newFile)
         
        saveAction = QtWidgets.QAction('Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save current file')        
        saveAction.triggered.connect(self.saveFile)
         
        openAction = QtWidgets.QAction('Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a file')
        openAction.triggered.connect(self.openFile)
         
        closeAction = QtWidgets.QAction('Close', self)
        closeAction.setShortcut('Ctrl+Q')
        closeAction.setStatusTip('Close Notepad')
        closeAction.triggered.connect(self.close) 

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(newAction)
        fileMenu.addAction(saveAction)
        fileMenu.addAction(openAction)
        fileMenu.addAction(closeAction)
         
        self.text = QtWidgets.QTextEdit(self)
         
        self.setCentralWidget(self.text)        
        self.setGeometry(300,300,300,300)
        self.setWindowTitle('Notepad')
        self.show()
         
    def newFile(self):
        self.text.clear()
         
    def saveFile(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File', os.getenv('HOME'))
        f = open(filename, 'w')
        filedata = self.text.toPlainText()        
        f.write(filedata)
        f.close()
         
         
    def openFile(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', os.getenv('HOME'))
        f = open(filename, 'r')
        filedata = f.read()
        self.text.setText(filedata)
        f.close()
         
def main():
    app = QtWidgets.QApplication(sys.argv)
    notepad = Notepad()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
